# docker-ubuntu-ssh

Ubuntu images with openssh-server, sudo, and a remote admin account added.


### Pull

`docker pull bduguid/ubuntu-ssh:tag`

### Remote Admin Account

SSH and Sudo account:

* username: radmin
* password: 1qaz2wsx

### Run

`docker run -d -P --name ubuntu-ssh-tag bduguid/ubuntu-ssh:tag`


##### Run Example for Chef Tutorial

One line: 

`docker run -d -P --expose 80 --expose 443 --name ubuntu-ssh-14 bduguid/ubuntu-ssh:14.04`